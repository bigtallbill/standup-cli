Standup
=======

Provides a simple commandline utility for managing daily standup reports.

Items from previous standups are carried forwards to today

## Day to day process

### Day1

 1. `standup new`

    - `# items carried forwards:`
 2. `standup add work "feature x"`
 3. `standup add fix "broken url in feature y"`
 4. `standup render`
    - `# Standup for Day1,`
    - `# Today:`
    - `# - [x] working on feature x`
    - `# - [x] fixing broken url in feature y`
 5. post rendered stand-up to chat
 6. `standup finish`
    - `# [1] work: feature x`
    - `# [2] fix: broken url in feature y`
    - `# enter item #: 2`
    - `# finished item #2`

### Day2

 1. `standup new`
    - `# items carried forwards:`
    - `# [1] work: feature x`
 2. `standup add do "documentation for feature x"`
 3. `standup render`
     - `# Standup for Day2,`
     - `# Yesterday:`
     - `# - [ ] working on feature x`
     - `# - [x] fixing broken url in feature y`
     - `# Today:`
     - `# - [ ] working on feature x`
     - `# - [ ] doing documentation for feature x`
 4. `standup add met "about the hard problem"`

## General Usage

Generate a new standup for today:

```shell
standup new
# items carried forwards:
# [1] work: feature y
```

Add items to your standup:

```shell
standup add work "feature x"
standup add chore "meeting with x"
standup add fix "broken url in feature y"
```

Finish an item:

```shell
standup finish
# [1] work: feature y
# [2] fix: bug x
# enter item #: 2
# finished item #2 from yesterday
```

Block an item

```shell
standup block
# [1] work: feature y
# [2] fix: bug x
# enter item #: 2
# blocked item #2 from yesterday
```

List your current items:

```shell
standup list
# Standup 2021-01-01

```

Render for posting to messaging systems:
```shell
standup render
# Standup for 2021-01-01,
# Yesterday:
# - [ ] worked on feature y
# - [x] fixed bug x
#
# Today:
# - [ ] working on feature y
# - [ ] fixing bug k
#
# Blocked:
# - bug x
```
