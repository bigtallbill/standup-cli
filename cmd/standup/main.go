package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"gopkg.in/alecthomas/kingpin.v2"
)

// kingpin commandline parsing.
var (
	app = kingpin.New("standup", "helps you write daily standups")

	newCmd = app.Command("new", "start a new standup")

	addCmd         = app.Command("add", "add a new item")
	addCategory    = addCmd.Arg("category", "the category for this item").Required().String()
	addDescription = addCmd.Arg("description", "description of this item").Required().String()

	finishCmd = app.Command("finish", "finishes an item interactively")
	blockCmd  = app.Command("block", "blocks an item interactively")
	render    = app.Command("render", "renders a standup based on the last")
)

const (
	dateFormat = "2006-01-02"
)

const (
	stateActive   = "active"
	stateFinished = "finished"
	stateBlocked  = "blocked"
)

func main() {
	configDir, err := os.UserConfigDir()
	if err != nil {
		log.Fatalf("for some reason couldn't find config dir: %s", err)
	}

	configDir = filepath.Join(configDir, "/standup")

	err = os.MkdirAll(configDir, os.ModePerm)
	if err != nil {
		log.Fatalf("couldnt create config directory in %s: %s", configDir, err)
	}

	configPath := filepath.Join(configDir, "/config.json")

	configFile, err := os.OpenFile(configPath, os.O_RDWR|os.O_CREATE, os.ModePerm)
	if err != nil {
		log.Fatalf("couldnt open config file in %s: %s", configPath, err)
	}

	err = createConfig(configFile, configDir)
	if err != nil {
		log.Fatalf("couldnt create config file in %s: %s", configPath, err)
	}

	var config Config

	err = json.NewDecoder(configFile).Decode(&config)
	if err != nil {
		log.Fatalf("could decode json config %s: %s", configPath, err)
	}

	configFile.Close()

	switch kingpin.MustParse(app.Parse(os.Args[1:])) {
	case newCmd.FullCommand():
		log.Print("begin new standup")
		// @todo protect against creating same file

		// load previous standup file (if exists)
		latestSu, _ := findLatestStandup(config, time.Now())

		// create new standup internal representation
		newSu := NewStandup()

		// carry forward active/blocked items
		for _, oldItem := range latestSu.Items {
			if oldItem.Status == stateActive || oldItem.Status == stateBlocked {
				newSu.Items = append(newSu.Items, oldItem)
			}
		}

		// save to disk
		err = saveStandupFile(newSu, newFileName(config, newSu.CreatedDate))
		if err != nil {
			log.Printf("couldnt save standup: %s", err)
		}
	case addCmd.FullCommand():
		log.Print("add")

		// load current standup
		latestSu, err := findLatestStandup(config, time.Now())
		if err != nil {
			log.Printf("couldnt find latest standup: %s", err)
		}

		// append item
		latestSu.Items = append(latestSu.Items, Item{
			Category:    *addCategory,
			Description: *addDescription,
			Status:      stateActive,
		})

		// save to disk
		err = saveStandupFile(latestSu, newFileName(config, latestSu.CreatedDate))
		if err != nil {
			log.Printf("couldnt save standup: %s", err)
		}
	case finishCmd.FullCommand():
		log.Print("finish")

		// load current standup
		latestSu, err := findLatestStandup(config, time.Now())
		if err != nil {
			log.Printf("couldnt find latest standup: %s", err)
		}

		if len(latestSu.Items) == 0 {
			log.Printf("no items in standup yet")
			break
		}

		// display list of active items
		latestSu.Items.Print()

		log.Printf("enter item number: ")

		// wait for input
		scanner := bufio.NewScanner(os.Stdin)
		if scanner.Scan() {
			itemIndex, err := strconv.Atoi(scanner.Text())
			if err != nil {
				log.Printf("input error: %s", err)
			} else {
				err = latestSu.Items.SetState(itemIndex, stateFinished)
				if err != nil {
					log.Printf("input error: %s", err)
				}
			}
		}

		// update relevant item

		// save to disk
		err = saveStandupFile(latestSu, newFileName(config, latestSu.CreatedDate))
		if err != nil {
			log.Printf("couldnt save standup: %s", err)
		}
	case blockCmd.FullCommand():
		log.Print("block")

		// load current standup
		latestSu, err := findLatestStandup(config, time.Now())
		if err != nil {
			log.Printf("couldnt find latest standup: %s", err)
		}

		if len(latestSu.Items) == 0 {
			log.Printf("no items in standup yet")
			break
		}

		// display list of active items
		latestSu.Items.Print()

		log.Printf("enter item number: ")

		// wait for input
		scanner := bufio.NewScanner(os.Stdin)
		if scanner.Scan() {
			itemIndex, err := strconv.Atoi(scanner.Text())
			if err != nil {
				log.Printf("input error: %s", err)
			} else {
				err = latestSu.Items.SetState(itemIndex, stateBlocked)
				if err != nil {
					log.Printf("input error: %s", err)
				}
			}
		}

		// update relevant item

		// save to disk
		err = saveStandupFile(latestSu, newFileName(config, latestSu.CreatedDate))
		if err != nil {
			log.Printf("couldnt save standup: %s", err)
		}
	case blockCmd.FullCommand():
		log.Print("block")

		// load current standup
		latestSu, err := findLatestStandup(config, time.Now())
		if err != nil {
			log.Printf("couldnt find latest standup: %s", err)
		}

		if len(latestSu.Items) == 0 {
			log.Printf("no items in standup yet")
			break
		}

		// display list of active items
		latestSu.Items.Print()

		log.Printf("enter item number: ")

		// wait for input
		scanner := bufio.NewScanner(os.Stdin)
		if scanner.Scan() {
			itemIndex, err := strconv.Atoi(scanner.Text())
			if err != nil {
				log.Printf("input error: %s", err)
			} else {
				err = latestSu.Items.SetState(itemIndex, stateBlocked)
				if err != nil {
					log.Printf("input error: %s", err)
				}
			}
		}

		// update relevant item

		// save to disk
		err = saveStandupFile(latestSu, newFileName(config, latestSu.CreatedDate))
		if err != nil {
			log.Printf("couldnt save standup: %s", err)
		}
	case render.FullCommand():
		log.Print("render")

		// load current standup
		latestSu, err := findLatestStandup(config, time.Now())
		if err != nil {
			log.Printf("couldnt find latest standup: %s", err)
		}

		// find the standup made prior to latest
		priorSu, err := findLatestStandup(config, latestSu.CreatedDate.AddDate(0, 0, -1))
		if err != nil {
			log.Printf("couldnt find prior standup: %s", err)
		}

		renderStandup(os.Stdout, CombinedStandup{
			Latest: &latestSu,
			Prior:  &priorSu,
		})
	}
}

func renderCheck(status string) string {
	if status == stateActive {
		return " "
	}

	if status == stateFinished {
		return "x"
	}

	return " "
}

func renderStandup(w io.Writer, standup CombinedStandup) {
	bw := bufio.NewWriter(w)

	_, _ = bw.WriteString(fmt.Sprintf("Happy %s,\n", standup.Latest.CreatedDate.Format("Monday")))

	if standup.Prior != nil {
		_, _ = bw.WriteString(fmt.Sprintf("Yesterday:\n"))

		for _, item := range standup.Prior.Items {
			_, _ = bw.WriteString(fmt.Sprintf("  - [%s] %s: %s\n", renderCheck(item.Status), item.Category, item.Description))
		}
	}

	_, _ = bw.WriteString(fmt.Sprintf("Today:\n"))

	for _, item := range standup.Latest.Items {
		_, _ = bw.WriteString(fmt.Sprintf("  - [%s] %s: %s\n", renderCheck(item.Status), item.Category, item.Description))
	}

	var blocked = standup.Latest.Items.ByStatus(stateBlocked)
	if len(blocked) > 0 {
		_, _ = bw.WriteString(fmt.Sprintf("Blocked:\n"))

		for _, item := range blocked {
			_, _ = bw.WriteString(fmt.Sprintf("  - [%s] %s: %s\n", renderCheck(item.Status), item.Category, item.Description))
		}
	}

	bw.Flush()
}

func findLatestStandup(c Config, currentTime time.Time) (Standup, error) {

	files, err := ioutil.ReadDir(c.StandupDir)
	if err != nil {
		return Standup{}, fmt.Errorf("cannot read standups from %s: %w", c.StandupDir, err)
	}

	var scannedFileCount = 0

	for scannedFileCount < 10 {
		currentFile := newFileName(c, currentTime)
		currentFilename := filepath.Base(currentFile)

		for _, file := range files {
			if strings.Contains(file.Name(), currentFilename) {

				su, err := loadStandupFile(filepath.Join(c.StandupDir, file.Name()))
				if err != nil {
					return Standup{}, fmt.Errorf("err loading: %w", err)
				}

				return su, nil
			}
		}

		currentTime = currentTime.AddDate(0, 0, -1)
		scannedFileCount++
	}

	return Standup{}, fmt.Errorf("cannot file latest standup")
}

func loadStandupFile(filePath string) (Standup, error) {
	file, err := os.OpenFile(filePath, os.O_RDONLY, os.ModePerm)
	if err != nil {
		return Standup{}, fmt.Errorf("err reading file %s: %w", filePath, err)
	}

	defer file.Close()

	var su Standup

	err = json.NewDecoder(file).Decode(&su)
	if err != nil {
		return Standup{}, fmt.Errorf("err decoding standup: %w", err)
	}

	return su, nil
}

func saveStandupFile(standup Standup, filePath string) error {
	file, err := os.OpenFile(filePath, os.O_RDWR|os.O_TRUNC|os.O_CREATE, os.ModePerm)
	if err != nil {
		return fmt.Errorf("err reading file %s: %w", filePath, err)
	}

	defer file.Close()

	err = json.NewEncoder(file).Encode(standup)
	if err != nil {
		return fmt.Errorf("err decoding standup: %w", err)
	}

	return nil
}

func newFileName(c Config, date time.Time) string {
	return filepath.Join(c.StandupDir, fmt.Sprintf("%s_standup.json", date.Format(dateFormat)))
}

type Item struct {
	Category    string `json:"category"`
	Description string `json:"description"`
	Status      string `json:"status"`
}

type Items []Item

func (i Items) SetState(idx int, newState string) error {
	if idx > len(i)-1 {
		return fmt.Errorf("item id %d out of range", idx)
	}

	i[idx].Status = newState

	return nil
}

func (i Items) ByStatus(status string) Items {
	var filtered Items

	for _, item := range i {
		if item.Status == status {
			filtered = append(filtered, item)
		}
	}

	return filtered
}

func (i Items) Print() {
	for idx, item := range i {
		log.Printf("[%d] %s: %s", idx, item.Category, item.Description)
	}
}

type Standup struct {
	CreatedDate time.Time `json:"created_date"`
	Items       Items     `json:"items"`
}

func NewStandup() Standup {
	return Standup{
		CreatedDate: time.Now(),
	}
}

type Config struct {
	StandupDir string `json:"standup_dir"`
}

func createConfig(configFile *os.File, configDir string) error {
	configStats, err := configFile.Stat()
	if err != nil {
		return fmt.Errorf("cannot get file stats: %w", err)
	}

	if configStats.Size() == 0 {
		standupsDir := filepath.Join(configDir, "standups")
		_ = os.MkdirAll(standupsDir, os.ModePerm)

		data, err := json.MarshalIndent(&Config{StandupDir: standupsDir}, "", " ")
		if err != nil {
			return fmt.Errorf("cannot marshal new config: %w", err)
		}

		_, err = configFile.Write(data)
		if err != nil {
			return fmt.Errorf("cannot write config file: %w", err)
		}

		_, err = configFile.Seek(0, 0)
		if err != nil {
			return fmt.Errorf("cannot seek config file after write: %w", err)
		}
	}

	return nil
}

type CombinedStandup struct {
	Latest *Standup
	Prior  *Standup
}
