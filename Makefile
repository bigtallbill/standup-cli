GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
BINARY_NAME=bin/standup

build:
	$(GOCLEAN) -cache && $(GOBUILD) -o $(BINARY_NAME) ./cmd/standup/main.go
	GOOS=darwin GOARCH=arm64 $(GOCLEAN) -cache && $(GOBUILD) -o $(BINARY_NAME)_darwin_arm64 ./cmd/standup/main.go
